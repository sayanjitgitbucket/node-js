/*
 *
 *Primary file for the uptimeApp
 *
 *
*/

//Dependecies for the App
const http = require('http');
const url = require('url');
//The server should response to all requests and send a welcome string
var server = http.createServer(function(req,res){

  //Get the Url and Parse it
  var urlParsed = url.parse(req.url,true);
  console.log(urlParsed);

  //Get the path excluding all other metadata
  var pathName = urlParsed.pathname;
  var trimPath = pathName.replace(/^\/+|\/+$/g,'');
  var method = req.method.toLowerCase();
  var queryString = urlParsed.query;
  var headers = req.headers;
  //console.log to server
  //console.log("The path to check -> "+trimPath+" by method: "+method +" and the queries are: "+ queryString);
  console.log(queryString);
  console.log(headers);
  //respond to user
  res.end("We got your url!!\n");

});

//Start the server and listen to port 3000
server.listen(3000,function(){
  console.log("The server is listening on port 3000");
})
